﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace CSharpTowerOfHanoi
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			TestTower.run();
			StateTest.run();
			BestFirstSearchTest.run();
			BreadthFirstSearchTest.run();
			DeepFirstSearchTest.run();
		}
	}

	class Step
	{
		public readonly string from;

		public readonly string to;

		public readonly int disk;

		public Step(string from, string to, int disk)
		{
			this.from = from;
			this.to = to;
			this.disk = disk;
		}

		public override string ToString()
		{
			return $"{this.disk} between {this.from}:{this.to}";
		}
	}

	class DeepStep : Step
	{
		private int level;

		private DeepStep(string from, string to, int disk, int level): base(from, to, disk) {
			this.level = level;
		}

		public DeepStep(Step step, int level): this(step.from, step.to, step.disk, level) {

		}

		public override string ToString()
		{
			return base.ToString() + " in level: " + this.level;
		}
	}

 	class ChangeStateAction
	{
		private StateInterface fromState;

		private StateInterface nextState;

		private Step step;

		public ChangeStateAction(StateInterface fromState, StateInterface nextState, Step step) {
			this.fromState = fromState;
			this.nextState = nextState;
			this.step = step;
		}

		public StateInterface getFromState()
		{
			return this.fromState;
		}

		public StateInterface getNextState()
		{
			return this.nextState;
		}

		public Step getStep()
		{
			return this.step;
		}
	}

	abstract class CommonBreadthFirstSearch : CommonStateSearcher
	{
		protected Dictionary<string, ChangeStateAction> hashChangeActionMap = new Dictionary<string, ChangeStateAction>();

		public CommonBreadthFirstSearch(StateInterface beginState, StateInterface endState): base(beginState, endState)
		{
		}

		public override bool solve()
		{
			bool found = this.search();

			if (found) {
				string hash = this.endState.getHash();

				List<Step> stepList = new List<Step>();

				while (this.hashChangeActionMap.ContainsKey(hash)) {
					ChangeStateAction action = this.hashChangeActionMap[hash];

					stepList.Add(action.getStep());

					hash = action.getFromState().getHash();
				}

				stepList.Reverse();

				this.stepList = stepList;
			}

			return found;
		}

		abstract protected bool search();
	}

	class BreadthFirstSearch : CommonBreadthFirstSearch
	{
		public BreadthFirstSearch(StateInterface beginState, StateInterface endState): base(beginState, endState)
		{
		}

		protected override bool search()
		{
			Stack <StateInterface> stack = new Stack<StateInterface>();
			stack.Push(this.beginState);
			this.addPastState(this.beginState);

			while (stack.Count > 0) {
				StateInterface currentState = stack.Pop();
				++this.count;

				if (this.isEndState(currentState)) {
					return true;
				}

				List<ChangeStateAction> getPossibleActions = this.getPossibleActions(currentState);
				foreach (ChangeStateAction possibleAction in getPossibleActions) {
					StateInterface nextState = possibleAction.getNextState();
					this.hashChangeActionMap.Add(nextState.getHash(), possibleAction);
					stack.Push(nextState);
				}
			}

			return false;
		}
	}

	class BestFirstSearch : CommonBreadthFirstSearch
	{
		public BestFirstSearch(StateInterface beginState, StateInterface endState): base(beginState, endState)
		{
		}

		protected override bool search()
		{
			SortedList <double, StateInterface> priorityStack = new SortedList<double, StateInterface>();
			priorityStack.Add(0, this.beginState);
			this.addPastState(this.beginState);

			while (priorityStack.Count > 0) {
				int index = 0;
				StateInterface currentState = priorityStack.Values[index];
				priorityStack.RemoveAt(index);
				++this.count;

				if (this.isEndState(currentState)) {
					return true;
				}

				List<ChangeStateAction> getPossibleActions = this.getPossibleActions(currentState);
				foreach (ChangeStateAction possibleAction in getPossibleActions) {
					StateInterface nextState = possibleAction.getNextState();
					this.hashChangeActionMap.Add(nextState.getHash(), possibleAction);
					double distance = State.getDistanceBetween(nextState, this.endState);

					// Если эта дистанция уже есть в очереди то повторное добавление вызовет ошибку
					// пробует добавить чуть поздне
					while (priorityStack.ContainsKey(distance)) {
						distance += 0.000001;
					}

					priorityStack.Add(distance, nextState);
				}
			}

			return false;
		}
	}

	class DeepFirstSearch : CommonStateSearcher
	{
		private bool found = false;

		private List<Step> reverseStepList = new List<Step>();

		public DeepFirstSearch(StateInterface beginState, StateInterface endState): base(beginState, endState)
		{
		}

		public override bool solve()
		{
			this.solveRecursive(this.beginState, 0, 1);

			if (this.found) {
				this.reverseStepList.Reverse();

				this.stepList = this.reverseStepList;
			}

			return this.found;
		}

		private int solveRecursive(StateInterface currentState, int level, int count)
		{
			if (this.isEndState(currentState)) {
				this.found = true;
				this.count = count;

				return 0;
			}

			List<ChangeStateAction> possibleActions = this.getPossibleActions(currentState);
			foreach (ChangeStateAction possibleAction in possibleActions) {
				int result = this.solveRecursive(possibleAction.getNextState(), level + 1, count + 1);

				if (this.found) {
					this.reverseStepList.Add(
						new DeepStep(possibleAction.getStep(), level)
					);
					return 0;
				}

				count = result + 1;
			}

			return count;
		}
	}

	abstract class CommonStateSearcher
	{
		protected StateInterface beginState;

		protected StateInterface endState;

		protected int count;

		protected List<Step> stepList;

		private HashSet<string> pastStateMap = new HashSet<string>();

		public CommonStateSearcher(StateInterface beginState, StateInterface endState)
		{
			this.beginState = beginState;
			this.endState = endState;
		}

		abstract public bool solve();

		public int getCount()
		{
			return this.count;
		}

		public List<Step> getStepList()
		{
			return this.stepList;
		}

		protected List<ChangeStateAction> getPossibleActions(StateInterface currentState)
		{
			string[] indexes = currentState.getTowerIndexes();
			List<ChangeStateAction> result = new List<ChangeStateAction>();

			foreach (string fromTowerIndex in indexes) {
				foreach (string toTowerIndex in indexes) {
					if (fromTowerIndex != toTowerIndex) {
						if (currentState.canMoveBetween(fromTowerIndex, toTowerIndex)) {
							StateInterface possibleAction = currentState.clone();

							int disk = possibleAction.move(fromTowerIndex, toTowerIndex);

							if (this.isPastState(possibleAction)) {
								continue;
							}

							this.addPastState(possibleAction);

							result.Add(new ChangeStateAction(
								currentState,
								possibleAction,
								new Step(fromTowerIndex, toTowerIndex, disk)
							));
						}
					}
				}
			}

			return result;
		}

		protected bool isEndState(StateInterface state)
		{
			return this.endState.getHash() == state.getHash();
		}

		protected bool isPastState(StateInterface state)
		{
			return this.pastStateMap.Contains(state.getHash());
		}

		protected void addPastState(StateInterface state)
		{
			this.pastStateMap.Add(state.getHash());
		}
	}

	class FreezeState : AbstractState
	{
		public FreezeState(Dictionary <string, Tower> towerMap): base(towerMap)
		{

		}

		public override int move(string fromTowerIndex, string toTowerIndex)
		{
			throw new Exception("This state is freeze");
		}
	}

	class State : AbstractState
	{
		public State(Dictionary <string, Tower> towerMap): base(towerMap)
		{

		}

		public override int move(string fromTowerIndex, string toTowerIndex)
		{
			Tower from = this.towerMap[fromTowerIndex];
			Tower to = this.towerMap[toTowerIndex];
			int disk = from.takeDisk();
			to.putDisk(disk);
			this.dropHash();
			return disk;
		}

		private void dropHash()
		{
			this.hash = null;
		}

		// Чем меньше дистанция тем ближе эти состояния
		// Данный алгоритм для дистанции был сделан экспериментальным путем
		// (Так как после копирования исходного алгоритма количество шагов увеличилось)
		public static double getDistanceBetween(StateInterface fromState, StateInterface toState)
		{
			Dictionary <string, int[]> fromTowerMap = fromState.getCurrent();
			Dictionary <string, int[]> toTowerMap = toState.getCurrent();

			double distance = 0;

			// Чтобы алгоритм был универсальным рассчитываем количество дисков
			int diskCount = 0;
			foreach (string key in fromTowerMap.Keys) {
				diskCount += fromTowerMap[key].Length;
			}

			foreach (string key in fromTowerMap.Keys) {
				int[] fromTowerDisks = fromTowerMap[key];
				int[] toTowerDisks = toTowerMap[key];

				int diff = State.getMulti(fromTowerDisks, diskCount) - State.getMulti(toTowerDisks, diskCount);

				// Если одна из башен пустая, а на другой есть диски то это должно больше влияеть на дистанцию
				if (fromTowerDisks.Length == 0 || toTowerDisks.Length == 0) {
					diff *= 2;
				}

				distance += diff * diff;
			}

			return distance;
		}

		private static int getMulti(int[] disks, int diskCount)
		{
			int result = 0;

			for (int index = 0; index < disks.Length; ++index) {
				// Чем выше диск тем меньше его значения для расчета дистанции
				result += disks[index] * (diskCount - index);
			}

			return result;
		}
	}

	abstract class AbstractState : StateInterface
	{
		/* type `string` is faster than `char` in this search */
		protected Dictionary<string, Tower> towerMap;

		protected string hash;

		public AbstractState(Dictionary <string, Tower> towerMap)
		{
			this.towerMap = towerMap;
		}

		public string getHash()
		{
			return string.IsNullOrEmpty(this.hash)
				? this.hash = this.calculateHash()
				: this.hash;
		}

		public bool canMoveBetween(string fromTowerIndex, string toTowerIndex)
		{
			Tower from = this.towerMap[fromTowerIndex];

			if (from.isEmpty()) {
				return false;
			}

			Tower to = this.towerMap[toTowerIndex];

			return to.isEmpty() || from.getTop() < to.getTop();
		}

		abstract public int move(string fromTowerIndex, string toTowerIndex);

		public string[] getTowerIndexes()
		{
			string[] keys = new string[this.towerMap.Keys.Count];
			this.towerMap.Keys.CopyTo(keys, 0);
			return keys;
		}

		protected string calculateHash()
		{
			string result = "";
			foreach(KeyValuePair<string, Tower> entry in this.towerMap) {
				result += entry.Key + ":" + entry.Value.getHash() + ";";
			}
			return result;
		}

		public StateInterface clone()
		{
			Dictionary<string, Tower> towerMap = new Dictionary<string, Tower>();

			foreach(KeyValuePair<string, Tower> entry in this.towerMap) {
				towerMap.Add(entry.Key, entry.Value.clone());
			}

			return new State(towerMap);
		}

		public Dictionary<string, int[]> getCurrent()
		{
			Dictionary<string, int[]> result = new Dictionary<string, int[]>();

			foreach (KeyValuePair<string, Tower> entry in this.towerMap) {
				result.Add(entry.Key, entry.Value.toArray());
			}

			return result;
		}
	}

	interface StateInterface
	{
		string getHash();

		bool canMoveBetween(string fromTowerIndex, string toTowerIndex);

		int move(string fromTowerIndex, string toTowerIndex);

		string[] getTowerIndexes();

		StateInterface clone();

		Dictionary<string, int[]> getCurrent();
	}

	class Tower
	{
		private List<int> disks = new List<int>();

		public Tower(int[] disks)
		{
			foreach (int disk in disks) {
				this.disks.Add(disk);
			}
		}

		public int getSize()
		{
			return this.disks.Count;
		}

		public bool isEmpty()
		{
			return this.getSize() == 0;
		}

		public int getTop()
		{
			return this.disks[this.getSize() - 1];
		}

		public void putDisk(int disk)
		{
			this.disks.Add(disk);
		}

		public int takeDisk()
		{
			int disk = this.getTop();

			this.disks.RemoveAt(this.getSize () - 1);

			return disk;
		}

		public string getHash()
		{
			string hash = "";
			foreach (int disk in this.disks) {
				hash += disk + ",";
			}
			return hash;
		}

		public Tower clone()
		{
			return new Tower(this.toArray());
		}

		public int[] toArray()
		{
			return this.disks.ToArray();
		}
	}

	class TestTower
	{
		public static void run()
		{
			Tower tower = new Tower (new int[]{8, 7});

			Console.WriteLine ("Start 'Tower' object tests!");
			Console.WriteLine ("Tower.getSize():                         " + (tower.getSize() == 2));
			Console.WriteLine ("Tower.isEmpty():                         " + (tower.isEmpty() == false));
			Console.WriteLine ("Tower.getTop():                          " + (tower.getTop() == 7));
			Console.WriteLine ("Tower.getHash():                         " + (tower.getHash() == "8,7,"));

			tower.putDisk(6);

			Console.WriteLine ("After tower.putDisk(), Tower.getSize():  " + (tower.getSize() == 3));
			Console.WriteLine ("Tower.takeDisk():                        " + (tower.takeDisk() == 6));
			Console.WriteLine ("After tower.takeDisk(), Tower.getSize(): " + (tower.getSize() == 2));
			Console.WriteLine ("Complete 'Tower' object tests!\n\n");
		}
	}

	class StateTest
	{
		public static void run()
		{
			State state = new State(new Dictionary<string, Tower>(){
				{"A", new Tower (new int[]{3, 2, 1})},
				{"B", new Tower (new int[]{})}
			});

			Console.WriteLine ("Start 'State' object tests!");
			Console.WriteLine ("State.getHash():                         " + (state.getHash() == "A:3,2,1,;B:;"));
			Console.WriteLine ("State.canMoveBetween():                  " + (state.canMoveBetween("A", "B") == true));
			Console.WriteLine ("State.canMoveBetween():                  " + (state.canMoveBetween("B", "A") == false));

			string[] towerIndexes = state.getTowerIndexes();
			Console.WriteLine ("State.getTowerIndexes():                 " + (towerIndexes.Length == 2 && towerIndexes[0] == "A" && towerIndexes[1] == "B"));

			Console.WriteLine ("State.move():                            " + (state.move("A", "B") == 1));

			Console.WriteLine ("After State.move(), State.getHash():     " + (state.getHash() == "A:3,2,;B:1,;"));

			Console.WriteLine ("State.getDistanceBetween():              " + (State.getDistanceBetween(state, state) == 0));

			Console.WriteLine ("Complete 'State' object tests!\n\n");
		}
	}

	class FirstSearchTest
	{
		private static int[] empty = new int[]{};
		private static int[] filled = new int[]{
			// 10,
			// 9,
			8,
			7, 6, 5, 4, 3, 2, 1
		};

		private static StateInterface beginState = new FreezeState(new Dictionary<string, Tower>(){
			{"A", new Tower (filled)},
			{"B", new Tower (empty)},
			{"C", new Tower (empty)},
			{"D", new Tower (empty)}
		});

		private static StateInterface endState = new FreezeState(new Dictionary<string, Tower>(){
			{"A", new Tower (empty)},
			{"B", new Tower (empty)},
			{"C", new Tower (empty)},
			{"D", new Tower (filled)}
		});

		public static StateInterface getBeginState()
		{
			return beginState;
		}

		public static StateInterface getEndState()
		{
			return endState;
		}

		public static bool isStepListCorrect(List<Step> steps)
		{
			StateInterface currentState = beginState.clone();
			foreach (Step step in steps) {
				if (
					currentState.canMoveBetween(step.from, step.to) 
					&& currentState.move(step.from, step.to) == step.disk
				) {
					continue;
				}

				return false;
			}

			return currentState.getHash() == endState.getHash();
		}

		public static void storeStepHistory(List<Step> steps, string fileName)
		{
			List<string> lines = new List<string>(steps.Count);
			foreach (Step step in steps) {
				lines.Add(step.ToString());
			}
			System.IO.File.WriteAllLines(fileName, lines);
		}
	}

	class DeepFirstSearchTest
	{
		public static void run()
		{
			int stackSize = 1024*1024*64;
			Thread th  = new Thread(
				DeepFirstSearchTest.runRecursion,
				stackSize
			);
			th.Start();
			th.Join();
		}

		public static void runRecursion()
		{
			StateInterface beginState = FirstSearchTest.getBeginState();
			StateInterface endState = FirstSearchTest.getEndState();

			Stopwatch sw = new Stopwatch();
			sw.Start();
			DeepFirstSearch deepFirstSearch = new DeepFirstSearch(beginState, endState);
			bool found = deepFirstSearch.solve();
			sw.Stop();

			Console.WriteLine ("Start 'DeepFirstSearch' object tests!");

			Console.WriteLine ("DeepFirstSearch.solve()                  " + found);
			Console.WriteLine ("Is correct steps:                        " + FirstSearchTest.isStepListCorrect(deepFirstSearch.getStepList()));
			Console.WriteLine ("Complete 'DeepFirstSearch' object tests!");
			Console.WriteLine ("count:" + deepFirstSearch.getCount());
			long microseconds = sw.ElapsedTicks / (Stopwatch.Frequency / (1000L*1000L));
			// ~ 1325839
			Console.WriteLine ("time(microseconds):" + microseconds + "\n\n");
			FirstSearchTest.storeStepHistory(deepFirstSearch.getStepList(), "deepFirstSearchOutput.txt");
		}
	}

	class BreadthFirstSearchTest
	{
		public static void run()
		{
			StateInterface beginState = FirstSearchTest.getBeginState();
			StateInterface endState = FirstSearchTest.getEndState();

			Stopwatch sw = new Stopwatch();
			sw.Start();
			BreadthFirstSearch breadthFirstSearch = new BreadthFirstSearch(beginState, endState);
			bool found = breadthFirstSearch.solve();
			sw.Stop();

			Console.WriteLine ("Start 'BreadthFirstSearch' object tests!");

			Console.WriteLine ("BreadthFirstSearch.solve()               " + found);
			Console.WriteLine ("Is correct steps:                        " + FirstSearchTest.isStepListCorrect(breadthFirstSearch.getStepList()));
			Console.WriteLine ("Complete 'BreadthFirstSearch' object tests!");
			Console.WriteLine ("count:" + breadthFirstSearch.getCount());
			long microseconds = sw.ElapsedTicks / (Stopwatch.Frequency / (1000L*1000L));
			// ~ 226231
			Console.WriteLine ("time(microseconds):" + microseconds + "\n\n");
			FirstSearchTest.storeStepHistory(breadthFirstSearch.getStepList(), "breadthFirstSearchOutput.txt");
		}
	}

	class BestFirstSearchTest
	{
		public static void run()
		{
			StateInterface beginState = FirstSearchTest.getBeginState();
			StateInterface endState = FirstSearchTest.getEndState();

			Stopwatch sw = new Stopwatch();
			sw.Start();
			BestFirstSearch bestFirstSearch = new BestFirstSearch(beginState, endState);
			bool found = bestFirstSearch.solve();
			sw.Stop();

			Console.WriteLine ("Start 'BestFirstSearch' object tests!");

			Console.WriteLine ("BestFirstSearch.solve()                  " + found);
			Console.WriteLine ("Is correct steps:                        " + FirstSearchTest.isStepListCorrect(bestFirstSearch.getStepList()));
			Console.WriteLine ("Complete 'BestFirstSearch' object tests!");
			Console.WriteLine ("count:" + bestFirstSearch.getCount());
			long microseconds = sw.ElapsedTicks / (Stopwatch.Frequency / (1000L*1000L));
			// ~ 226231
			Console.WriteLine ("time(microseconds):" + microseconds + "\n\n");
			FirstSearchTest.storeStepHistory(bestFirstSearch.getStepList(), "bestFirstSearchOutput.txt");
		}
	}
}
